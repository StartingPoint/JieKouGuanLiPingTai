package cn.zfgc.zface.service;


import cn.zfgc.base.api.DateTimeUtils;
import cn.zfgc.base.api.ObjectUtils;
import cn.zfgc.base.api.time.constant.DateTimeFormat;
import cn.zfgc.base.model.ReturnDataDO;
import cn.zfgc.zface.constant.CommonConstant;
import cn.zfgc.zface.entity.CompanyEntity;
import cn.zfgc.zface.entity.ProjectsEntity;
import cn.zfgc.zface.entity.ZfaceEntity;
import cn.zfgc.zface.service.data.FunctionDataService;
import cn.zfgc.zface.util.FaceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by lx on 2017/10/31.
 */
@Service
public class FunctionService {

    @Autowired
    FunctionDataService functionDataService;

    //显示接口
    public String getFaceList(){
        List<ZfaceEntity> zfaceEntityList = functionDataService.findAllFaceList();
        if(ObjectUtils.isNotNull(zfaceEntityList)){
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"成功！",zfaceEntityList);
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"没有内容！");
    }

    //添加接口
    public String addFace(Map<String, String[]> paramMaps){
        if(ObjectUtils.isNotNull(paramMaps)){
            ZfaceEntity zfaceEntity = ZfaceEntity.paraToFace(paramMaps);
            zfaceEntity.setFaceKey(FaceUtil.getFaceKey());
            if(ObjectUtils.isNotNull(zfaceEntity)){
                boolean flg = zfaceEntity.fieldsToAtts(zfaceEntity).save();
                if(flg){
                    return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"添加成功！");
                }
            }
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"添加失败！");
    }

    //修改接口
    public String updateFace(Map<String, String[]> paramMaps){
        if(ObjectUtils.isNotNull(paramMaps)){
            ZfaceEntity zfaceEntity = ZfaceEntity.paraToFace(paramMaps);
            if(ObjectUtils.isNotNull(zfaceEntity)){
                ZfaceEntity zface = functionDataService.fundFaceByKey(zfaceEntity.getFaceKey());
                if(ObjectUtils.isNotNull(zface)){
                    zface.setFaceKey(zfaceEntity.getFaceKey());
                    zface.setZfaceName(zfaceEntity.getZfaceName());
                    zface.setZfaceType(zfaceEntity.getZfaceType());
                    zface.setProjectID(zfaceEntity.getProjectID());
                    zface.setZfaceUrl(zfaceEntity.getZfaceUrl());
                    zface.setHttpType(zfaceEntity.getHttpType());
                    zface.setDataType(zfaceEntity.getDataType());
                    zface.setSoTimeout(zfaceEntity.getSoTimeout());
                    zface.setConnectTimeout(zfaceEntity.getConnectTimeout());
                    zface.setStatus(zfaceEntity.getStatus());
                    zface.setWebServicesNamespace(zfaceEntity.getWebServicesNamespace());
                    zface.setWebServicesMethod(zfaceEntity.getWebServicesMethod());
                    zface.setArrayParaName(zfaceEntity.getArrayParaName());
                    zface.setCharset(zfaceEntity.getCharset());
                    zface.setUpdateTime(DateTimeUtils.date2String(new Date(), DateTimeFormat.Format2.toString()));
                    boolean flg = zface.fieldsToAtts(zface).update();
                    if(flg){
                        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"修改成功！");
                    }
                }
            }
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"修改失败！");
    }

    //删除接口
    public String deleteFace(String faceKey){
        if(ObjectUtils.isNotNull(faceKey)){
           functionDataService.deleteFaceByKey(faceKey);
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"删除成功！");
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"删除失败！");
    }

    //获取公司列表
    public String getCompanyList(){
        List<CompanyEntity> companyEntityList = functionDataService.findAllCompanyList();
        if(ObjectUtils.isNotNull(companyEntityList)){
            for(CompanyEntity c : companyEntityList){
                List<ProjectsEntity> projectsEntityList = functionDataService.findProjectByCompanyID(c.getID());
                int faceSum = 0;
                for(ProjectsEntity p : projectsEntityList){
                    List<ZfaceEntity> zfaceEntityList = functionDataService.findFaceByProjectID(p.getID());
                    faceSum = faceSum+zfaceEntityList.size();
                }
                c.set(CommonConstant.PROJECT_SUM,projectsEntityList.size());
                c.set(CommonConstant.FACE_SUM,faceSum);
            }
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"成功！",companyEntityList);
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"没有内容！");
    }

    //添加公司
    public String addCompany(Map<String, String[]> paramMaps){
        if(ObjectUtils.isNotNull(paramMaps)){
            CompanyEntity companyEntity = CompanyEntity.paraToCompany(paramMaps);
            if(ObjectUtils.isNotNull(companyEntity)){
                boolean flg = companyEntity.fieldsToAtts(companyEntity).save();
                if(flg){
                    return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"添加成功！");
                }
            }
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"添加失败！");
    }

    //删除公司
    public String deleteCompany(String companyID){
        if(ObjectUtils.isNotNull(companyID)){
            List<ProjectsEntity> projectsEntityList = functionDataService.findProjectByCompanyID(companyID);
            if(ObjectUtils.isNotNull(projectsEntityList)){
                return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"当前公司下存在其他项目，禁止删除！");
            }
            functionDataService.deleteCompanyByID(companyID);
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"删除成功！");
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"删除失败！");
    }

    //获取项目
    public String getProject(String companyID){
        if(ObjectUtils.isNotNull(companyID)){
            List<ProjectsEntity> projectsEntityList = functionDataService.findProjectByCompanyID(companyID);
            if(ObjectUtils.isNotNull(projectsEntityList)){
                return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"成功！",projectsEntityList);
            }
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"没有内容！");
    }

    //添加项目
    public String addProject(Map<String, String[]> paramMaps){
        if(ObjectUtils.isNotNull(paramMaps)){
            ProjectsEntity projectsEntity = ProjectsEntity.paraToProject(paramMaps);
            if(ObjectUtils.isNotNull(paramMaps)){
                boolean flg = projectsEntity.fieldsToAtts(projectsEntity).save();
                if(flg){
                    return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"添加成功！");
                }
            }
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"添加失败！");
    }

    //删除项目
    public String deleteProject(String projectID){
        if(ObjectUtils.isNotNull(projectID)){
            List<ZfaceEntity> zfaceEntityList = functionDataService.findFaceByProjectID(projectID);
            if(ObjectUtils.isNotNull(zfaceEntityList)){
                return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"当前项目下存在其他接口，禁止删除！");
            }
            functionDataService.deleteProjectByID(projectID);
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"删除成功！");
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"删除失败！");
    }

}
