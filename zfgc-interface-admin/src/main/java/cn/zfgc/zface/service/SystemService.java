package cn.zfgc.zface.service;

import cn.zfgc.base.api.DateTimeUtils;
import cn.zfgc.base.api.ObjectUtils;
import cn.zfgc.base.api.time.constant.DateTimeFormat;
import cn.zfgc.base.model.ReturnDataDO;
import cn.zfgc.zface.entity.CharsetEntity;
import cn.zfgc.zface.entity.UserEntity;
import cn.zfgc.zface.service.data.SystemDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by lx on 2017/11/1.
 */
@Service
public class SystemService {

    @Autowired
    private SystemDataService systemDataService;

    //查询所有用户
    public String getUserList(){
        List<UserEntity> userEntityList = systemDataService.findAllUserList();
        if(ObjectUtils.isNotNull(userEntityList)){
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"成功！",userEntityList);
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"失败！");
    }

    //添加用户
    public String addUser(Map<String, String[]> paramMaps){
        UserEntity userEntity = UserEntity.paraToUser(paramMaps);
        UserEntity user = systemDataService.findUserByUserName(userEntity.getUserName());
        if(ObjectUtils.isNotNull(user)){
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"用户名已被使用！");
        }
        boolean flg = userEntity.fieldsToAtts(userEntity).save();
        if(flg){
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"添加成功！");
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"添加失败！");
    }

    //修改用户信息
    public String updateUser(Map<String, String[]> paramMaps){
        UserEntity userEntity = UserEntity.paraToUser(paramMaps);
        if(ObjectUtils.isNotNull(userEntity)){
            UserEntity user = systemDataService.findUserByUserName(userEntity.getUserName());
            user.setName(userEntity.getName());
            user.setSex(userEntity.getSex());
            user.setPhone(userEntity.getPhone());
            user.setEmail(userEntity.getEmail());
            user.setUpdateTime(DateTimeUtils.date2String(new Date(), DateTimeFormat.Format2.toString()));
            boolean flg = user.fieldsToAtts(user).update();
            if(flg){
                return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"修改成功！");
            }
        }
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"添加失败！");
    }

    //删除用户
    public String deleteUser(String userName){
        systemDataService.deleteUserByUserName(userName);
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"删除成功！");
    }

    public String  getCharList(){
        List<CharsetEntity> charsetEntityList=systemDataService.findAllCharset();
        return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"成功！",charsetEntityList);
    }
    public String addCharset(Map<String, String[]> paramMaps){
        CharsetEntity charsetEntity=CharsetEntity.paraToCharset(paramMaps);
        Boolean bool=charsetEntity.fieldsToAtts(charsetEntity).save();
        if(bool) {
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"添加成功！");
        }
        else{
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"添加失败！");
        }
    }
    public String upCharset(Map<String, String[]> paramMaps){
        CharsetEntity charsetEntity=CharsetEntity.paraToCharset(paramMaps);
        CharsetEntity charset=systemDataService.findCharsetById(charsetEntity.getID());
        charset.setCharsetName(charsetEntity.getCharsetName());
        charset.setCreateTime(DateTimeUtils.getCurrentDateWithFormatStr(DateTimeFormat.Format2));
        Boolean bool=charset.update();
        if(bool)
        {
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"修改成功！");
        }
        else{
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"修改失败！");
        }
    }
    public String delCharset(String id)
    {
        Boolean bool=systemDataService.delCharset(id);
        if(bool)
        {
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_TRUE_CODE,"删除成功！");
        }
        else{
            return ReturnDataDO.returnJson(ReturnDataDO.RETURN_ERROR_CODE,"删除失败！");
        }
    }

    //登录
    public UserEntity login(String userName,String password){
        return systemDataService.findUserByUserNameAndPassword(userName,password);
    }


}
