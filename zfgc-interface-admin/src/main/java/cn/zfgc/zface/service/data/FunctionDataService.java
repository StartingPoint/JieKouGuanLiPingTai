package cn.zfgc.zface.service.data;


import cn.zfgc.jfinal.plugin.activerecord.Db;
import cn.zfgc.zface.constant.CommonConstant;
import cn.zfgc.zface.entity.CompanyEntity;
import cn.zfgc.zface.entity.ProjectsEntity;
import cn.zfgc.zface.entity.ZfaceEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lx on 2017/10/31.
 */
@Service
public class FunctionDataService {

    //查询所有接口
    public List<ZfaceEntity> findAllFaceList(){
        return ZfaceEntity.DAO.find("select * from face where state = '"+ CommonConstant.NOT_DELETE+"'");
    }

    //根据faceKey查询接口
    public ZfaceEntity fundFaceByKey(String faceKey){
        return ZfaceEntity.DAO.findFirst("select * from face where faceKey ='"+faceKey+"' and state = '"+CommonConstant.NOT_DELETE+"'");
    }

    //根据faceKey删除接口
    public void deleteFaceByKey(String faceKey){
        Db.update("update face set state='"+CommonConstant.DELETE+"' where faceKey= '"+faceKey+"'");
    }

    //查询所有公司
    public List<CompanyEntity> findAllCompanyList(){
        return CompanyEntity.DAO.find("select * from company where state = '"+CommonConstant.NOT_DELETE+"'");
    }

    //根据公司ID查询所属项目
    public List<ProjectsEntity> findProjectByCompanyID(String companyID){
        return ProjectsEntity.DAO.find("select * from projects where companyID = '"+companyID+"' and state ='"+CommonConstant.NOT_DELETE+"'");
    }

    //删除公司
    public void deleteCompanyByID(String companyID){
        Db.update("update company set state='"+CommonConstant.DELETE+"' where ID= '"+companyID+"'");
    }

    //根据项目ID查询所属接口
    public List<ZfaceEntity> findFaceByProjectID(String projectID){
        return ZfaceEntity.DAO.find("select * from face where projectID ='"+projectID+"' and state = '"+CommonConstant.NOT_DELETE+"'");
    }

    //删除项目
    public void deleteProjectByID(String projectID){
        Db.update("update projects set state='"+CommonConstant.DELETE+"' where ID= '"+projectID+"'");
    }
}
