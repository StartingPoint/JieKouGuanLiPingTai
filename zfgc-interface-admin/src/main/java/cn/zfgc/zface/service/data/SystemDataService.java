package cn.zfgc.zface.service.data;

import cn.zfgc.base.api.DateTimeUtils;
import cn.zfgc.base.api.time.constant.DateTimeFormat;
import cn.zfgc.jfinal.plugin.activerecord.Db;
import cn.zfgc.zface.constant.CommonConstant;
import cn.zfgc.zface.entity.CharsetEntity;
import cn.zfgc.zface.entity.UserEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lx on 2017/11/1.
 */

@Service
public class SystemDataService {

    //根据账号查询用户信息
    public UserEntity findUserByUserName(String userName){
        return UserEntity.DAO.findFirst("select * from user where userName = '"+userName+"' and state ='"+ CommonConstant.NOT_DELETE+"'");
    }
    //查询所有用户
    public List<UserEntity> findAllUserList(){
        return UserEntity.DAO.find("select * from user where state = '"+ CommonConstant.NOT_DELETE+"'");
    }
    //根据账号删除用户
    public void deleteUserByUserName(String userName){
        Db.update("delete user set state='"+CommonConstant.DELETE+"' where userName = '"+userName+"'");
    }
    //查询字符集
    public List<CharsetEntity> findAllCharset(){
        return CharsetEntity.DAO.find("select * from charset");
    }
    //根据id查询字符信息
    public CharsetEntity findCharsetById(String id){
        return  CharsetEntity.DAO.findFirst("select * from charset where ID="+id);
    }
    //删除字符集
    public Boolean delCharset(String id){
        return  CharsetEntity.DAO.deleteById(id);
    }

    //
    public UserEntity findUserByUserNameAndPassword(String userName,String password){
        return UserEntity.DAO.findFirst("select * from user where userName= '"+userName+"' and password = '"+password+"'");
    }
}
