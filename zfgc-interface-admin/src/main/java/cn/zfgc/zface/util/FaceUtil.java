package cn.zfgc.zface.util;

import cn.zfgc.nutz.lang.Lang;

import java.util.Date;

/**
 * Created by lx on 2017/11/2.
 */
public class FaceUtil {

    //获取faceKey
    public static String getFaceKey(){
        String time = String.valueOf(new Date().getTime());
        String str = Lang.md5(time);
        return str;
    }

}
