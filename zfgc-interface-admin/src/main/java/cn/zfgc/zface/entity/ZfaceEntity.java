package cn.zfgc.zface.entity;

import cn.zfgc.base.api.DateTimeUtils;
import cn.zfgc.base.api.JsonUtils;
import cn.zfgc.base.api.time.constant.DateTimeFormat;
import cn.zfgc.jfinal.plugin.activerecord.Model;
import cn.zfgc.zface.constant.CommonConstant;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

/**
 * @author caoyuan on 2017/10/9.
 */

@Getter
@Setter
public class ZfaceEntity extends Model<ZfaceEntity> {
    public final static ZfaceEntity DAO = new ZfaceEntity().dao();

    private String ID;

    private String faceKey;

    private String zfaceName;

    private String zfaceType;

    private String zfaceUrl;

    private String httpType;

    private String dataType;

    private String soTimeout;

    private String connectTimeout;

    private String status;

    private String webServicesNamespace;

    private String webServicesMethod;

    private String arrayParaName;

    private String projectID;

    private String charset;

    private String createTime;

    private String updateTime;

    private String state;

    private String createUserName;

    public static ZfaceEntity paraToFace(Map<String, String[]> paramMaps){
        ZfaceEntity zfaceEntity = new ZfaceEntity();
        zfaceEntity.setFaceKey(paramMaps.get("faceKey")[0]);
        zfaceEntity.setZfaceName(paramMaps.get("zfaceName")[0]);
        zfaceEntity.setZfaceType(paramMaps.get("zfaceType")[0]);
        zfaceEntity.setZfaceUrl(paramMaps.get("zfaceUrl")[0]);
        zfaceEntity.setHttpType(paramMaps.get("httpType")[0]);
        zfaceEntity.setDataType(paramMaps.get("dataType")[0]);
        zfaceEntity.setSoTimeout(paramMaps.get("soTimeout")[0]);
        zfaceEntity.setConnectTimeout(paramMaps.get("connectTimeout")[0]);
        zfaceEntity.setStatus(paramMaps.get("status")[0]);
        zfaceEntity.setWebServicesNamespace(paramMaps.get("webServicesNamespace")[0]);
        zfaceEntity.setWebServicesMethod(paramMaps.get("webServicesMethod")[0]);
        zfaceEntity.setArrayParaName(paramMaps.get("arrayParaName")[0]);
        zfaceEntity.setProjectID(paramMaps.get("projectID")[0]);
        zfaceEntity.setCharset(paramMaps.get("charset")[0]);
        zfaceEntity.setCreateUserName(paramMaps.get("createUserName")[0]);
        zfaceEntity.setCreateTime(DateTimeUtils.date2String(new Date(), DateTimeFormat.Format2.toString()));
        zfaceEntity.setUpdateTime(DateTimeUtils.date2String(new Date(), DateTimeFormat.Format2.toString()));
        zfaceEntity.setState(CommonConstant.NOT_DELETE);
        return zfaceEntity;
    }


    @Override
    public <T extends Model> T fieldsToAtts(T t) {
        Map attrs = super.getAttrs();
        attrs.put("faceKey", this.getFaceKey());
        attrs.put("zfaceName", this.getZfaceName());
        attrs.put("zfaceType", this.getZfaceType());
        attrs.put("zfaceUrl", this.getZfaceUrl());
        attrs.put("httpType", this.getHttpType());
        attrs.put("dataType", this.getDataType());
        attrs.put("soTimeout", this.getSoTimeout());
        attrs.put("connectTimeout", this.getConnectTimeout());
        attrs.put("status", this.getStatus());
        attrs.put("webServicesNamespace", this.getWebServicesNamespace());
        attrs.put("webServicesMethod", this.getWebServicesMethod());
        attrs.put("arrayParaName", this.getArrayParaName());
        attrs.put("projectID", this.getProjectID());
        attrs.put("charset", this.getCharset());
        attrs.put("createUserName", this.getCreateUserName());
        attrs.put("createTime", this.getCreateTime());
        attrs.put("updateTime", this.getUpdateTime());
        attrs.put("state", this.getState());
        return t;
    }

    @Override
    public String toString() {
        return "face{" + JsonUtils.toJson(this) + "}";
    }
}
