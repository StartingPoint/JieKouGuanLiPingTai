package cn.zfgc.zface.entity;

import cn.zfgc.base.api.DateTimeUtils;
import cn.zfgc.base.api.JsonUtils;
import cn.zfgc.base.api.time.constant.DateTimeFormat;
import cn.zfgc.jfinal.plugin.activerecord.Model;
import cn.zfgc.zface.constant.CommonConstant;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

@Setter
@Getter
public class CharsetEntity extends Model<CharsetEntity> {
    public static final CharsetEntity DAO=new CharsetEntity().dao();

    private String ID ;

    private String charsetName;

    private String createTime;

    private String updateTime;

    private String state;

    private String createUserName;

    public static CharsetEntity paraToCharset(Map<String, String[]> paramMaps){
        CharsetEntity charsetEntity = new CharsetEntity();
        charsetEntity.setCharsetName(paramMaps.get("charsetName")[0]);
        charsetEntity.setCreateUserName(paramMaps.get("createUserName")[0]);
        charsetEntity.setCreateTime(DateTimeUtils.date2String(new Date(), DateTimeFormat.Format2.toString()));
        charsetEntity.setUpdateTime(DateTimeUtils.date2String(new Date(), DateTimeFormat.Format2.toString()));
        charsetEntity.setState(CommonConstant.NOT_DELETE);
        return charsetEntity;
    }

    @Override
    public <T extends Model> T fieldsToAtts(T t) {
        Map attrs = super.getAttrs();
        attrs.put("charsetName", this.getCharsetName());
        attrs.put("createTime", this.getCreateTime());
        attrs.put("updateTime", this.getUpdateTime());
        attrs.put("state", this.getState());
        attrs.put("createUserName", this.getCreateUserName());
        return t;
    }

    @Override
    public String toString() {
        return "CharsetEntity{" + JsonUtils.toJson(this) + '}';
    }
}
