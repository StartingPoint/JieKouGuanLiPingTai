package cn.zfgc.zface.entity;

import cn.zfgc.base.api.DateTimeUtils;
import cn.zfgc.base.api.JsonUtils;
import cn.zfgc.base.api.time.constant.DateTimeFormat;
import cn.zfgc.jfinal.plugin.activerecord.Model;
import cn.zfgc.zface.constant.CommonConstant;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

/**
 * @author caoyuan
 */
@Getter
@Setter
public class ProjectsEntity extends Model<ProjectsEntity> {
    public final static ProjectsEntity DAO = new ProjectsEntity().dao();

    private String ID;

    private String projectName;

    private String createTime;

    private String updateTime;

    private String state;

    private String createUserName;

    public static ProjectsEntity paraToProject(Map<String, String[]> paramMaps){
        ProjectsEntity projectsEntity = new ProjectsEntity();
        projectsEntity.setProjectName(paramMaps.get("projectName")[0]);
        projectsEntity.setCreateUserName(paramMaps.get("createUserName")[0]);
        projectsEntity.setCreateTime(DateTimeUtils.date2String(new Date(), DateTimeFormat.Format2.toString()));
        projectsEntity.setUpdateTime(DateTimeUtils.date2String(new Date(), DateTimeFormat.Format2.toString()));
        projectsEntity.setState(CommonConstant.NOT_DELETE);
        return projectsEntity;
    }

    @Override
    public <T extends Model> T fieldsToAtts(T t) {
        Map attrs = super.getAttrs();
        attrs.put("projectName", this.getProjectName());
        attrs.put("createTime", this.getCreateTime());
        attrs.put("updateTime", this.getUpdateTime());
        attrs.put("state", this.getState());
        attrs.put("createUserName", this.getCreateUserName());
        return t;
    }

    @Override
    public String toString() {
        return "Projects{" + JsonUtils.toJson(this) + '}';
    }
}
