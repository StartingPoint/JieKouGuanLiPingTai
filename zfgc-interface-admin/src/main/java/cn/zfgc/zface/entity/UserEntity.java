package cn.zfgc.zface.entity;

import cn.zfgc.base.api.DateTimeUtils;
import cn.zfgc.base.api.JsonUtils;
import cn.zfgc.base.api.time.constant.DateTimeFormat;
import cn.zfgc.jfinal.plugin.activerecord.Model;
import cn.zfgc.zface.constant.CommonConstant;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Map;

/**
 * Created by lx on 2017/10/31.
 */
@Setter
@Getter
public class UserEntity extends Model<UserEntity> {

    public final static UserEntity DAO = new UserEntity().dao();

    private String ID;

    private String name;

    private String sex;

    private String phone;

    private String email;

    private String userName;

    private String password;

    private String createTime;

    private String updateTime;

    private String state;

    private String power;

    private String createUserName;


    public static UserEntity paraToUser(Map<String, String[]> paramMaps){
        UserEntity userEntity = new UserEntity();
        userEntity.setName(paramMaps.get("name")[0]);
        userEntity.setSex(paramMaps.get("sex")[0]);
        userEntity.setPhone(paramMaps.get("phone")[0]);
        userEntity.setEmail(paramMaps.get("email")[0]);
        userEntity.setUserName(paramMaps.get("userName")[0]);
        userEntity.setPassword(paramMaps.get("password")[0]);
        userEntity.setPower(paramMaps.get("power")[0]);
        userEntity.setCreateUserName(paramMaps.get("createUserName")[0]);
        userEntity.setCreateTime(DateTimeUtils.date2String(new Date(), DateTimeFormat.Format2.toString()));
        userEntity.setUpdateTime(DateTimeUtils.date2String(new Date(), DateTimeFormat.Format2.toString()));
        userEntity.setState(CommonConstant.NOT_DELETE);
        return userEntity;
    }

    @Override
    public <T extends Model> T fieldsToAtts(T t) {
        Map attrs = super.getAttrs();
        attrs.put("name", this.getName());
        attrs.put("sex", this.getSex());
        attrs.put("phone", this.getPhone());
        attrs.put("email", this.getEmail());
        attrs.put("userName", this.getUserName());
        attrs.put("password", this.getPassword());
        attrs.put("power", this.getPower());
        attrs.put("createTime", this.getCreateTime());
        attrs.put("updateTime", this.getUpdateTime());
        attrs.put("state", this.getState());
        attrs.put("createUserName", this.getCreateUserName());
        return t;
    }

    @Override
    public String toString() {
        return "user{" + JsonUtils.toJson(this) + "}";
    }


}
