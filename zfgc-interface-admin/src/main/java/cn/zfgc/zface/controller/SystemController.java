package cn.zfgc.zface.controller;

import cn.zfgc.base.api.ObjectUtils;
import cn.zfgc.base.model.ReturnDataDO;
import cn.zfgc.zface.controller.base.BaseController;
import cn.zfgc.zface.entity.CharsetEntity;
import cn.zfgc.zface.entity.UserEntity;
import cn.zfgc.zface.service.SystemService;

/**
 * Created by lx on 2017/11/1.
 */
public class SystemController extends BaseController {

    private SystemService systemService = ctx.getBean(SystemService.class);

    public void getAllCharset(){
        String res=systemService.getCharList();
        renderJson(res);
    }
    public void upCharset(){
        CharsetEntity charsetEntity=new CharsetEntity();
        String res=systemService.upCharset(getParaMap());
        renderJson(res);
    }
    public void addCharset(){
        CharsetEntity charsetEntity=new CharsetEntity();
        String res=systemService.addCharset(getParaMap());
        renderJson(res);
    }
    public void delCharset(){
        String id=getPara("id");
        String res=systemService.delCharset(id);
        renderJson(res);
    }

    //显示用户
    public void userListAction(){
        String json = systemService.getUserList();
        renderJson(json);
    }

    //添加用户
    public void addUserAction(){
        String json = systemService.addUser(getParaMap());
        renderJson(json);
    }

    //修改用户信息
    public void updateUserAction(){
        String json = systemService.updateUser(getParaMap());
        renderJson(json);
    }

    //删除用户
    public void deleteUserAction(){
        String userName = getPara("userName");
        String json = systemService.deleteUser(userName);
        renderJson(json);
    }

    //登录
    public void login(){
        String userName = getPara("userName");
        String password = getPara("password");
        if(ObjectUtils.isNull(userName) || ObjectUtils.isNull(password)){
            setAttr("error","用户名或密码不能为空！");
            render("/login.jsp");
            return;
        }
        UserEntity userEntity = systemService.login(userName,password);
        if(ObjectUtils.isNull(userEntity)){
            setAttr("error","用户名或密码错误！");
            render("/login.jsp");
        }
    }

    //进入登录页面
    public void index(){
        render("/login.jsp");
    }

}
