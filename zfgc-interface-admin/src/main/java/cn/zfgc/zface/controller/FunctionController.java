package cn.zfgc.zface.controller;

import cn.zfgc.zface.controller.base.BaseController;
import cn.zfgc.zface.service.FunctionService;

/**
 * Created by lx on 2017/10/31.
 */
public class FunctionController extends BaseController{

    private FunctionService functionService = ctx.getBean(FunctionService.class);

    //显示接口
    public void faceAction(){
        String json = functionService.getFaceList();
        renderJson(json);
    }

    //添加接口
    public void addFaceAction(){
        String json = functionService.addFace(getParaMap());
        renderJson(json);
    }

    //修改接口
    public void updateFaceAction(){
        String json = functionService.updateFace(getParaMap());
        renderJson(json);
    }

    //删除接口
    public void deleteFaceAction(){
        String faceKey = getPara("faceKey");
        String json = functionService.deleteFace(faceKey);
        renderJson(json);
    }

    //显示公司
    public void companyListAction(){
        String json = functionService.getCompanyList();
        renderJson(json);
    }

    //添加公司
    public void addCompanyAction(){
        String json = functionService.addCompany(getParaMap());
        renderJson(json);
    }

    //删除公司
    public void deleteCompanyAction(){
        String companyID = getPara("companyID");
        String json = functionService.deleteCompany(companyID);
        renderJson(json);
    }

    //显示项目
    public void projectListAction(){
        String companyID = getPara("companyID");
        String json = functionService.getProject(companyID);
        renderJson(json);
    }

    //添加项目
    public void addProjectAction(){
        String json = functionService.addProject(getParaMap());
        renderJson(json);
    }

    //删除项目
    public void deleteProjectAction(){
        String projectID = getPara("projectID");
        String json = functionService.deleteProject(projectID);
        renderJson(json);
    }

}
