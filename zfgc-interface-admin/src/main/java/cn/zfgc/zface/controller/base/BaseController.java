package cn.zfgc.zface.controller.base;

import cn.zfgc.jfinal.core.Controller;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;

/**
 * Created by wangwang 2017/1/19.
 */
public class BaseController extends Controller {

    protected final static WebApplicationContext webApplicationContext = ContextLoader.getCurrentWebApplicationContext();

    protected final static ServletContext servletContext = webApplicationContext.getServletContext();

    protected final static ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
}
