package cn.zfgc.zface.constant;

/**
 * Created by lx on 2017/10/31.
 */
public interface CommonConstant {
    //删除
    String NOT_DELETE = "0";
    String DELETE = "1";

    //权限
    String FACE_POWER = "0";
    String SYSTEM_POWER = "1";

    //项目数量字段
    String PROJECT_SUM = "projectSum";

    //接口数量字段
    String FACE_SUM = "faceSum";
}
