package cn.zfgc.zface.jfinal.config;

import cn.zfgc.conf.core.ZfgcConfClient;
import cn.zfgc.conf.core.model.DataBaseSource;
import cn.zfgc.jfinal.config.*;
import cn.zfgc.jfinal.plugin.activerecord.ActiveRecordPlugin;
import cn.zfgc.jfinal.plugin.activerecord.dialect.MysqlDialect;
import cn.zfgc.jfinal.plugin.druid.DruidPlugin;
import cn.zfgc.jfinal.render.ViewType;
import cn.zfgc.jfinal.template.Engine;
import cn.zfgc.spring.context.SpringContextHolder;
import cn.zfgc.zface.controller.*;
import cn.zfgc.zface.entity.*;
import com.alibaba.druid.filter.stat.StatFilter;

public class InitConfig extends JFinalConfig {
    @Override
    public void configConstant(Constants constants) {
        constants.setViewType(ViewType.JSP);
        constants.setDevMode(Boolean.parseBoolean("true"));
    }

    @Override
    public void configRoute(Routes routes) {
        routes.add("/function", FunctionController.class);
        routes.add("/system", SystemController.class);
    }

    @Override
    public void configEngine(Engine engine) {

    }

    @Override
    public void configPlugin(Plugins me) {
        // oracle数据源
        DataBaseSource dataSource = SpringContextHolder.getBean(DataBaseSource.class);
        DruidPlugin druidPlugin = new DruidPlugin(dataSource.getUrl(), dataSource.getUsername(), dataSource.getPassword(), "com.mysql.jdbc.Driver");
        druidPlugin.setTestOnBorrow(false);
        druidPlugin.addFilter(new StatFilter());
        me.add(druidPlugin);
        ActiveRecordPlugin mysql = new ActiveRecordPlugin(druidPlugin);

        mysql.setDialect(new MysqlDialect());
        mysql.addMapping("face", ZfaceEntity.class);
        mysql.addMapping("projects", ProjectsEntity.class);
        mysql.addMapping("company", CompanyEntity.class);
        mysql.addMapping("charset", CharsetEntity.class);
        mysql.addMapping("user", UserEntity.class);
        me.add(mysql);
    }

    @Override
    public void configInterceptor(Interceptors interceptors) {

    }
}
