<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>绽放工场接口管理平台</title>
    <link rel="stylesheet" href="/static/css/reset.css"/>
    <link rel="stylesheet" href="/static/css/loginStyle.css"/>
</head>
<body style="-ms-behavior: url('/static/css/backgroundsize.min.htc');behavior: url('/static/css/backgroundsize.min.htc');">

<form id="form" name="form" method="post" action="/system/login" autocomplete="off">
    <input type="text" style="position: absolute;top: -9999px;"/>
    <input type="password" style="position: absolute;top: -9999px;"/>

    <img class="logo" src="/static/img/logo.png" alt=""/>
    <p class="title">绽放工场接口管理平台</p>
    <p style="color: red">${error}</p>
    <div class="row">
        <img class="leftIcon" src="/static/img/icon-user.png" alt=""/>
        <input type="text" name="userName" id="user" value="" placeholder="请输入用户名" autocomplete="off">
    </div>
    <div class="row">
        <img class="leftIcon" src="/static/img/icon-password.png" alt=""/>
        <input type="text" id="tx" style="display: none;color: #cccccc;" value="请输入密码" autocomplete="off">
        <input type="password" name="password" id="password" placeholder="请输入密码" value="" autocomplete="off">
    </div>
    <input type="submit" class="submit" name="submit" value="登录">
</form>


<script>
    var passTx=document.getElementById("tx");
    //兼容IE9-的placeholder属性。
    var funPlaceholder = function(element) {
        var placeholder = '';
        if (element && !("placeholder" in document.createElement("input")) && (placeholder = element.getAttribute("placeholder"))) {
            element.onfocus = function() {
                if (this.value === placeholder) {
                    this.value = "";
                }
                this.style.color = '#ffffff';
            };
            element.onblur = function() {
                if (this.value === "") {
                    this.value = placeholder;
                    this.style.color = '#cccccc';
                }

            };

            //样式初始化
            if (element.value === "") {
                element.value = placeholder;
                element.style.color = '#cccccc';
            }

            //兼容password的显示提醒。
            if(element.getAttribute("type")=="password"){
                //初始化
                element.style.display = "none";
                passTx.style.display = "block";

                passTx.onfocus = function () {
                    this.style.display = "none";
                    element.style.display = "block";
                    element.value = "";
                    element.focus();
                };
                element.onblur = function () {
                    if (this.value != "")
                        return;
                    this.style.display = "none";
                    passTx.style.display = "block";
                }

            }
        }
    };
    funPlaceholder(document.getElementById("user"));
    funPlaceholder(document.getElementById("password"));

</script>
</body>
</html>
