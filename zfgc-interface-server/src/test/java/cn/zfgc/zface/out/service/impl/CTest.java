package cn.zfgc.zface.out.service.impl;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.zfgc.zface.out.service.ZFaceService;

public class CTest {
    public static void main(String[] args) throws Exception {


        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"/dubbo-demo-c.xml" });
        context.start();
        ZFaceService demoService = (ZFaceService) context.getBean("demoService"); // obtain proxy object for remote invocation

        String json = "<requestXml>"
                + "<![CDATA[<?xml version=\"1.0\" encoding=\"GBK\"  standalone=\"yes\" ?><Packet type=\"REQUEST\" version=\"1.0\"><Head><RequestType>Q06</RequestType><FlowInTime>2017-10-24 14:56:47</FlowInTime><RequesterCode>weixin</RequesterCode><PassWord>123456</PassWord><IP>10.192.21.88</IP></Head><Body><BaseInfo><ComCode>0204001304</ComCode><ModelCName>BH7161GAY</ModelCName><VinNo>35RF3F3FYT3S33WS2</VinNo><EngineNum>34675322698745</EngineNum><PlateNum>京A23187</PlateNum></BaseInfo></Body></Packet>]]>"
                + "</requestXml>";

        String hello = demoService.doRequest("bb2c459df0caccdd65ab4d03c1ea4f9f", json);
        System.out.println("---------------" + hello); // show the result
    }
}
