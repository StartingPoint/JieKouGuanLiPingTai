package cn.zfgc.zface.out.service.impl;

import cn.zfgc.zface.out.service.ZFaceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/META-INF/spring/weicloud-provider.xml" })
public class ZfaceServiceImplTest {

    @Autowired
    private ZFaceService zFaceService;

    @Test
    public void doRequest() throws Exception {

        try {
            System.in.read(); // press any key to exit
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}