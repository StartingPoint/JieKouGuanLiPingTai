package cn.zfgc.zface.constant;

import lombok.Getter;

/**
 * @author caoyuan on 2017/10/11. 定义接口类型的枚举
 */
public enum HttpTypeEnum {
    GET("1"), POST("0");

    @Getter
    private String value;

    HttpTypeEnum(String value) {
        this.value = value;
    }

}
