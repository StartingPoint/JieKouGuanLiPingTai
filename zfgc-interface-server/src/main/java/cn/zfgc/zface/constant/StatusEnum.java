package cn.zfgc.zface.constant;

/**
 * @author caoyuan on 2017/10/11. 状态枚举类型
 */
public enum StatusEnum {
    // 1 可用,2不可用;
    IS_ABLE(1), UN_ABLE(2);

    public int value;

    StatusEnum(int value) {
        this.value = value;
    }

}
