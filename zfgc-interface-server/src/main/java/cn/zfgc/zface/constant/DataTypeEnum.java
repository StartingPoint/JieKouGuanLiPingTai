package cn.zfgc.zface.constant;

import lombok.Getter;

/**
 * @author caoyuan
 */
public enum DataTypeEnum {
    Xml("0"), Json("1"), KeyValue("2"), NOPara("3"), Array("4");

    DataTypeEnum(String dataType) {
        this.dataType = dataType;
    }

    @Getter
    private String dataType;

}
