package cn.zfgc.zface.constant;

import lombok.Getter;

/**
 * @author caoyuan on 2017/10/11. 定义接口类型的枚举
 */
public enum InterfaceTypeEnum {
    // 接口的类型:1为httpget请求,2为httppost请求,0为webservice
    HTTP("0"), WEBSERVICE("1");

    @Getter
    private String value;

    InterfaceTypeEnum(String value) {
        this.value = value;
    }

}
