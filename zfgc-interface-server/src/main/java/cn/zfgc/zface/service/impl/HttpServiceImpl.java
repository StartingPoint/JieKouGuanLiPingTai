package cn.zfgc.zface.service.impl;

import java.util.Date;
import java.util.HashMap;

import org.springframework.stereotype.Service;

import cn.zfgc.base.api.CharsetNames;
import cn.zfgc.base.api.DateTimeUtils;
import cn.zfgc.base.api.Exceptions;
import cn.zfgc.base.api.HttpClient;
import cn.zfgc.base.api.ObjectUtils;
import cn.zfgc.base.api.StringUtils;
import cn.zfgc.base.api.time.constant.DateTimeFormat;
import cn.zfgc.zface.model.Charset;
import cn.zfgc.zface.model.ErrorLog;
import cn.zfgc.zface.model.Zface;
import cn.zfgc.zface.service.HttpService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author caoyuan on 2017/10/11.
 */
@Service
@Slf4j
class HttpServiceImpl implements HttpService {
    /**
     * http接口通过get方式获取接口结果
     *
     * @param inter
     * @return
     */
    @Override
    public String doGet(Zface inter) {
        if (ObjectUtils.isNull(inter)) {
            return null;
        }

        String response = "";
        try {

            response = HttpClient.get(inter.getStr("zfaceUrl")).connectTimeout(inter.getInt("connectTimeout")).readTimeout(inter.getInt("soTimeout")).execute(true).asString();
        } catch (Exception e) {
            new ErrorLog().set("faceKey", inter.getStr("faceKey")).set("zfaceName", inter.getStr("zfaceName"))
                    .set("errorTime", DateTimeUtils.date2Str(new Date(), DateTimeFormat.Format2)).set("errorMsg", Exceptions.getStackTraceAsString(e)).save();
            log.error("", e);
        }
        return response;
    }

    /**
     * http接口通过get方式获取接口结果
     *
     * @param inter
     * @return
     */
    @Override
    public String doPost(Zface inter) {
        if (ObjectUtils.isNull(inter)) {
            return null;
        }

        String response = "";
        try {
            response = HttpClient.post(inter.getStr("zfaceUrl")).connectTimeout(inter.getInt("connectTimeout")).readTimeout(inter.getInt("soTimeout")).execute(true).asString();
        } catch (Exception e) {
            new ErrorLog().set("faceKey", inter.getStr("faceKey")).set("zfaceName", inter.getStr("zfaceName"))
                    .set("errorTime", DateTimeUtils.date2Str(new Date(), DateTimeFormat.Format2)).set("errorMsg", Exceptions.getStackTraceAsString(e)).save();
            log.error("", e);
        }

        return response;

    }

    @Override
    public String doPost(Zface inter, HashMap<String, String> data) {
        String response = "";

        try {
            if (ObjectUtils.isNull(data)) {
                response = HttpClient.post(inter.getStr("zfaceUrl")).connectTimeout(inter.getInt("connectTimeout")).readTimeout(inter.getInt("soTimeout")).execute(true).asString();
            } else {
                response = HttpClient.post(inter.getStr("zfaceUrl")).queryString(data).execute(true).asString();
            }
        } catch (Exception e) {
            new ErrorLog().set("faceKey", inter.getStr("faceKey")).set("zfaceName", inter.getStr("zfaceName"))
                    .set("errorTime", DateTimeUtils.date2Str(new Date(), DateTimeFormat.Format2)).set("errorMsg", Exceptions.getStackTraceAsString(e)).save();
            log.error("", e);

        }

        return response;
    }

    @Override
    public String doGet(Zface inter, HashMap<String, String> data) {
        String response = "";

        try {
            if (ObjectUtils.isNull(data)) {
                response = HttpClient.get(inter.getStr("zfaceUrl")).connectTimeout(inter.getInt("connectTimeout")).readTimeout(inter.getInt("soTimeout")).execute(true).asString();
            } else {
                response = HttpClient.get(inter.getStr("zfaceUrl")).queryString(data).execute(true).asString();
            }
        } catch (Exception e) {
            new ErrorLog().set("faceKey", inter.getStr("faceKey")).set("zfaceName", inter.getStr("zfaceName"))
                    .set("errorTime", DateTimeUtils.date2Str(new Date(), DateTimeFormat.Format2)).set("errorMsg", Exceptions.getStackTraceAsString(e)).save();
            log.error("", e);
        }

        return response;
    }

    @Override
    public String doJson(Zface inter, String data) {
        if (ObjectUtils.isNull(inter) || StringUtils.isBlank(data)) {
            return null;
        }

        String charset;

        String charsetType = inter.getStr("charset");

        if (StringUtils.isBlank(charsetType)) {
            charset = CharsetNames.UTF_8.toString();
        } else {
            charset = getCharsetByConf(charsetType);
        }

        String response = "";
        try {
            response = HttpClient.textBody(inter.getStr("zfaceUrl")).charset(charset).json(data).connectTimeout(inter.getInt("connectTimeout"))
                    .readTimeout(inter.getInt("soTimeout")).execute(true).asString();
        } catch (Exception e) {
            new ErrorLog().set("faceKey", inter.getStr("faceKey")).set("zfaceName", inter.getStr("zfaceName"))
                    .set("errorTime", DateTimeUtils.date2Str(new Date(), DateTimeFormat.Format2)).set("errorMsg", Exceptions.getStackTraceAsString(e)).save();
            log.error("", e);
        }

        return response;

    }

    @Override
    public String doXml(Zface inter, String data) {
        if (ObjectUtils.isNull(inter) || StringUtils.isBlank(data)) {
            return null;
        }

        String charset;

        String charsetType = inter.getStr("charset");

        if (StringUtils.isBlank(charsetType)) {
            charset = CharsetNames.UTF_8.toString();
        } else {
            charset = getCharsetByConf(charsetType);
        }

        String response = "";
        try {
            response = HttpClient.textBody(inter.getStr("zfaceUrl")).xml(data).charset(charset).connectTimeout(inter.getInt("connectTimeout"))
                    .readTimeout(inter.getInt("soTimeout")).execute(true).asString();
        } catch (Exception e) {
            new ErrorLog().set("faceKey", inter.getStr("faceKey")).set("zfaceName", inter.getStr("zfaceName"))
                    .set("errorTime", DateTimeUtils.date2Str(new Date(), DateTimeFormat.Format2)).set("errorMsg", Exceptions.getStackTraceAsString(e)).save();
            log.error("", e);
        }

        return response;
    }

    private static String getCharsetByConf(String charsetType) {
        String charset = CharsetNames.UTF_8.toString();
        ;
        // 查找对应的编码表

        Charset charsetData = Charset.DAO.findFirst("select * from charset where ID='" + charsetType + "'");

        if (ObjectUtils.isNotNull(charsetData)) {
            charset = charsetData.getStr("charset");
        }

        return charset;
    }
}
