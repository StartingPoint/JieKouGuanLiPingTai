package cn.zfgc.zface.service;

import cn.zfgc.zface.model.Zface;

/**
 * @author caoyuan on 2017/10/11.
 */
public interface WebServicesService {
    /**
     * 调用webservice接口获取结果
     * 
     * @param inter
     * @param data
     * @return
     */
    String doJson(Zface inter, String data);

    /**
     * 调用webservice接口获取结果
     *
     * @param inter
     * @param data
     * @return
     */
    String doXml(Zface inter, String data);

    /**
     * 调用webservice接口获取结果
     *
     * @param inter
     * @param data
     * @return
     */
    String doArray(Zface inter, String data);

}
