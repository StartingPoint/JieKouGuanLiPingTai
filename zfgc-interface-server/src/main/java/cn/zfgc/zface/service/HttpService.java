package cn.zfgc.zface.service;

import java.util.HashMap;

import cn.zfgc.zface.model.Zface;

/**
 * @author caoyuan on 2017/10/11.
 */
public interface HttpService {
    /**
     * http接口通过get方式获取接口结果
     * 
     * @return
     */
    String doGet(Zface inter);

    /**
     * http接口通过get方式获取接口结果
     * 
     * @return
     */
    String doPost(Zface inter);

    /**
     * http接口通过post访问方式获取接口
     * 
     * @param inter
     * @param data
     * @return
     */
    String doPost(Zface inter, HashMap<String, String> data);

    /**
     * http接口通过get方式获取接口结果
     *
     * @param data
     * @return
     */
    String doGet(Zface inter, HashMap<String, String> data);

    /**
     * http接口json访问方式获取接口
     *
     * @param inter
     * @param data
     * @return
     */
    String doJson(Zface inter, String data);

    /**
     * http接口xml访问方式获取接口
     *
     * @param inter
     * @param data
     * @return
     */
    String doXml(Zface inter, String data);

}
