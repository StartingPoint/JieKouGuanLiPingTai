package cn.zfgc.zface.jfinal.conf;

import com.alibaba.druid.filter.stat.StatFilter;

import cn.zfgc.conf.core.model.DataBaseSource;
import cn.zfgc.jfinal.config.Constants;
import cn.zfgc.jfinal.config.JFinalSpringConfig;
import cn.zfgc.jfinal.config.Plugins;
import cn.zfgc.jfinal.plugin.activerecord.ActiveRecordPlugin;
import cn.zfgc.jfinal.plugin.activerecord.dialect.MysqlDialect;
import cn.zfgc.jfinal.plugin.druid.DruidPlugin;
import cn.zfgc.jfinal.template.Engine;
import cn.zfgc.spring.context.SpringContextHolder;
import cn.zfgc.zface.model.Company;
import cn.zfgc.zface.model.ErrorLog;
import cn.zfgc.zface.model.Log;
import cn.zfgc.zface.model.Projects;
import cn.zfgc.zface.model.Zface;

public class InitConfig extends JFinalSpringConfig {
    @Override
    public void configConstant(Constants constants) {

    }

    @Override
    public void configEngine(Engine engine) {

    }

    @Override
    public void configPlugin(Plugins me) {
        // oracle数据源
        DataBaseSource dataSource = SpringContextHolder.getBean(DataBaseSource.class);
        DruidPlugin druidPlugin = new DruidPlugin(dataSource.getUrl(), dataSource.getUsername(), dataSource.getPassword(), "com.mysql.jdbc.Driver");
        druidPlugin.setTestOnBorrow(false);
        druidPlugin.addFilter(new StatFilter());
        me.add(druidPlugin);
        ActiveRecordPlugin mysql = new ActiveRecordPlugin(druidPlugin);
        me.add(mysql);
        mysql.setDialect(new MysqlDialect());
        mysql.addMapping("face", Zface.class);
        mysql.addMapping("log", Log.class);
        mysql.addMapping("projects", Projects.class);
        mysql.addMapping("company", Company.class);
        mysql.addMapping("errorLog", ErrorLog.class);

    }
}
