package cn.zfgc.zface.model;

import cn.zfgc.base.api.JsonUtils;
import cn.zfgc.jfinal.plugin.activerecord.Model;
import lombok.Getter;
import lombok.Setter;

/**
 * @author caoyuan on 2017/10/9.
 */

@Getter
@Setter
public class Zface extends Model<Zface> {
    public final static Zface DAO = new Zface().dao();

    private String ID;

    private String faceKey;

    private String zfaceName;

    private String zfaceType;

    private String projectID;

    private String zfaceUrl;

    private String httpType;

    private String dataType;

    private String soTimeout;

    private String connectTimeout;

    private String status;

    private String createTime;

    private String updateTime;

    private String webServicesNamespace;

    private String webServicesMethod;

    private String arrayParaName;

    @Override
    public String toString() {
        return "Interface{" + JsonUtils.toJson(this) + "}";
    }
}
