package cn.zfgc.zface.model;

import java.util.Map;

import cn.zfgc.base.api.JsonUtils;
import cn.zfgc.jfinal.plugin.activerecord.Model;
import lombok.Getter;
import lombok.Setter;

/**
 * @author caoyuan
 */
@Getter
@Setter
public class Company extends Model<Company> {
    public final static Company DAO = new Company().dao();

    private String ID;

    private String companyName;

    @Override
    public <T extends Model> T fieldsToAtts(T t) {
        Map attrs = super.getAttrs();
        attrs.put("ID", this.getID());
        attrs.put("companyName", this.getCompanyName());
        return t;
    }

    @Override
    public String toString() {
        return "Company{" + JsonUtils.toJson(this) + '}';
    }
}
