package cn.zfgc.zface.model;

import cn.zfgc.base.api.JsonUtils;
import cn.zfgc.jfinal.plugin.activerecord.Model;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorLog extends Model<ErrorLog> {
    public final static ErrorLog DAO = new ErrorLog().dao();

    private String ID;

    private String faceKey;

    private String faceName;

    private String errorTime;

    @Override
    public String toString() {
        return "ErrorLog{" + JsonUtils.toJson(this) + '}';
    }
}
