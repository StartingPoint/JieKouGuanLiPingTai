package cn.zfgc.zface.model;

import java.util.Map;

import cn.zfgc.base.api.JsonUtils;
import cn.zfgc.jfinal.plugin.activerecord.Model;
import lombok.Getter;
import lombok.Setter;

/**
 * @author caoyuan
 */
@Getter
@Setter
public class Projects extends Model<Projects> {
    public final static Projects DAO = new Projects().dao();

    private String ID;

    private String projectName;

    @Override
    public <T extends Model> T fieldsToAtts(T t) {
        Map attrs = super.getAttrs();
        attrs.put("ID", this.getID());
        attrs.put("companyName", this.getProjectName());
        return t;
    }

    @Override
    public String toString() {
        return "Projects{" + JsonUtils.toJson(this) + '}';
    }
}
