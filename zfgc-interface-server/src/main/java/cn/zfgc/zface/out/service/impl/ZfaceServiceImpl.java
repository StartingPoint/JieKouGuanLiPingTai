package cn.zfgc.zface.out.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cn.zfgc.zface.service.WebServicesService;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;

import cn.zfgc.base.api.DateTimeUtils;
import cn.zfgc.base.api.JsonUtils;
import cn.zfgc.base.api.ObjectUtils;
import cn.zfgc.base.api.StringUtils;
import cn.zfgc.base.api.time.constant.DateTimeFormat;
import cn.zfgc.zface.constant.DataTypeEnum;
import cn.zfgc.zface.constant.HttpTypeEnum;
import cn.zfgc.zface.constant.InterfaceTypeEnum;
import cn.zfgc.zface.model.Log;
import cn.zfgc.zface.model.Zface;
import cn.zfgc.zface.out.service.ZFaceService;
import cn.zfgc.zface.service.HttpService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author caoyuan. 判断用户请求的是什么类型的接口并调用对应的接口的服务进行获取结果
 */
@Service(timeout = 30000)
@Slf4j
public class ZfaceServiceImpl implements ZFaceService {

    @Autowired
    private HttpService httpService;

    @Autowired
    private WebServicesService webServicesService;

    /**
     * @param fKey
     * @param data
     * @return
     */
    @Override
    public String doRequest(String fKey, HashMap<String, String> data) {
        Zface zface;
        String responseData = "";
        // 如果没有找到接口配置信息，则返回为空
        List<Zface> zfaces = Zface.DAO.find("select * from face where faceKey='" + fKey + "'");

        if (ObjectUtils.isNotNull(zfaces)) {
            zface = zfaces.get(0);
        } else {
            return "";
        }

        if (StringUtils.equals(zface.getStr("dataType"), DataTypeEnum.KeyValue.getDataType())) {
            // http
            if (StringUtils.equals(zface.getStr("zfaceType"), InterfaceTypeEnum.HTTP.getValue())) {

                // get
                if (StringUtils.equals(zface.getStr("httpType"), HttpTypeEnum.GET.getValue())) {
                    responseData = httpService.doGet(zface, data);
                } else {
                    // post
                    responseData = httpService.doPost(zface, data);
                }
            }
        }

        new Log().set("faceKey", zface.getStr("faceKey")).set("input", JsonUtils.toJson(data)).set("output", responseData)
                .set("faceTime", DateTimeUtils.date2Str(new Date(), DateTimeFormat.Format2)).save();

        return responseData;
    }

    /**
     *
     * @param fKey
     * @param data
     * @return
     */
    @Override
    public String doRequest(String fKey, String data) {
        Zface zface;
        String responseData = "";

        List<Zface> zfaces = Zface.DAO.find("select * from face where faceKey='" + fKey + "'");
        if (ObjectUtils.isNotNull(zfaces)) {
            zface = zfaces.get(0);
        } else {
            return "";
        }
        // http
        if (StringUtils.equals(zface.getStr("zfaceType"), InterfaceTypeEnum.HTTP.getValue())) {
            if (StringUtils.equals(zface.getStr("dataType"), DataTypeEnum.Json.getDataType())) {
                responseData=httpService.doJson(zface, data);
            }

            if (StringUtils.equals(zface.getStr("dataType"), DataTypeEnum.Xml.getDataType())) {
                responseData=httpService.doXml(zface, data);
            }
        }

        //webServices
        if (StringUtils.equals(zface.getStr("zfaceType"), InterfaceTypeEnum.WEBSERVICE.getValue())) {
            if (StringUtils.equals(zface.getStr("dataType"), DataTypeEnum.Json.getDataType())) {
                responseData= webServicesService.doJson(zface, data);
            }

            if (StringUtils.equals(zface.getStr("dataType"), DataTypeEnum.Xml.getDataType())) {
                responseData=webServicesService.doXml(zface, data);
            }

            if (StringUtils.equals(zface.getStr("dataType"), DataTypeEnum.Array.getDataType())) {
                responseData=webServicesService.doArray(zface, data);
            }
        }



        new Log().set("faceKey", zface.getStr("faceKey")).set("input", JsonUtils.toJson(data)).set("output", responseData)
                .set("faceTime", DateTimeUtils.date2Str(new Date(), DateTimeFormat.Format2)).save();

        return responseData;
    }

    @Override
    public String doRequest(String fKey) {
        Zface zface;
        String responseData = "";

        List<Zface> zfaces = Zface.DAO.find("select * from face where faceKey='" + fKey + "'");
        if (ObjectUtils.isNotNull(zfaces)) {
            zface = zfaces.get(0);
        } else {
            return "";
        }

        // http
        if (StringUtils.equals(zface.getStr("zfaceType"), InterfaceTypeEnum.HTTP.getValue())) {
            if (StringUtils.equals(zface.getStr("dataType"), DataTypeEnum.NOPara.getDataType())) {
                // get
                if (StringUtils.equals(zface.getStr("httpType"), HttpTypeEnum.GET.getValue())) {
                    responseData = httpService.doGet(zface);
                    return responseData;
                } else {
                    // post
                    responseData = httpService.doPost(zface);
                    return responseData;
                }
            }

        }

        new Log().set("faceKey", zface.getStr("faceKey")).set("output", responseData).set("faceTime", DateTimeUtils.date2Str(new Date(), DateTimeFormat.Format2)).save();

        return responseData;
    }

}
