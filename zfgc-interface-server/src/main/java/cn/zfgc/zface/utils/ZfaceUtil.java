package cn.zfgc.zface.utils;

import java.util.HashMap;
import java.util.Map;

import cn.zfgc.base.api.StringUtils;

/**
 * @author caoyuan
 */
public class ZfaceUtil {

    /**
     * @param keyValue 必须符合 "x=1,y=2"
     * @return
     */
    public static Map<String, String> keyValueToMap(String keyValue) {
        if (!StringUtils.hasLength(keyValue)) {
            return null;
        }

        HashMap<String, String> kvMap = new HashMap<>();

        String[] k1 = StringUtils.split(keyValue, ",");

        for (int i = 0; i < k1.length; i++) {
            String[] k2 = StringUtils.split(k1[i], "=");
            kvMap.put(k2[0], k2[1]);
        }

        return kvMap;
    }
}
