package cn.zfgc.zface.out.service;

import java.util.HashMap;

public interface ZFaceService {

    /**
     * 键值对请求的对应方法，只支持http形式的，可以对应get或者post
     * 
     * @param fKey
     * @param data
     * @return
     */
    String doRequest(String fKey, HashMap<String, String> data);

    /**
     * json、xml、array格式的请求方法，可以支持http和webservices的方式
     * 
     * @param fKey
     * @param data
     * @return
     */
    String doRequest(String fKey, String data);

    /**
     * 无参数的请求方法，只支持http形式的，可以对应get或者post
     * 
     * @param fKey
     * @return
     */
    String doRequest(String fKey);
}
